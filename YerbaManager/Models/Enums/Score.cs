﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Cors;

namespace YerbaManager.Models.Enums
{
    public enum Score
    {
        [Display(Name = "Bad")]
        Bad = 1,
        [Display(Name = "Not bad")]
        NotBad = 2,
        [Display(Name = "Ok")]
        Ok = 3,
        [Display(Name = "Good")]
        Good = 4,
        [Display(Name = "Better")]
        Better = 5,
        [Display(Name = "Excelent")]
        Excelent = 6
    }
}
