﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YerbaManager.Models.Enums;

namespace YerbaManager.Models
{
    public class Yerba
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Producer Producer { get; set; }
        public int Jar { get; set; }
        public Score Score { get; set; }
    }
}
