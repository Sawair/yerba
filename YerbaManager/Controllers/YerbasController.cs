using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using YerbaManager.Data;
using YerbaManager.Models;
using YerbaManager.Models.YerbasViewModel;

namespace YerbaManager.Controllers
{
    public class YerbasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public YerbasController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Yerbas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Yerbas
                .Include(y => y.Producer)
                .ToListAsync());
        }

        // GET: Yerbas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var yerba = await _context.Yerbas
                .Include(y => y.Producer)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (yerba == null)
            {
                return NotFound();
            }

            return View(yerba);
        }

        // GET: Yerbas/Create
        public IActionResult Create()
        {
            ViewData["Producers"] = _context.Producer.ToListAsync();
            return View();
        }

        // POST: Yerbas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Producer,Jar,Score")] YerbaViewModel model)
        {
            if (ModelState.IsValid)
            {
                var yerba = new Yerba
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    Jar = model.Jar,
                    Score = model.Score,
                    Producer = await _context.Producer.FirstAsync(p => p.Id == model.Producer)
                };
                _context.Add(yerba);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public static async Task<List<SelectListItem>> GetProducersList(Task<List<Producer>> producers)
        {
            return (await producers).AsParallel().Select(p => new SelectListItem(){ Text=p.Name, Value = p.Id.ToString()}).ToList();
        }

        // GET: Yerbas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var yerba = await _context.Yerbas.Include(y => y.Producer).SingleOrDefaultAsync(m => m.Id == id);
            if (yerba == null)
            {
                return NotFound();
            }
            ViewData["Producers"] = _context.Producer.ToListAsync();
            return View(new YerbaViewModel
            {
                Id = yerba.Id,
                Name = yerba.Name,
                Description = yerba.Description,
                Producer = yerba.Producer.Id,
                Jar = yerba.Jar,
                Score = yerba.Score
            });
        }

        // POST: Yerbas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Producer,Jar,Score")] YerbaViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var yerba = new Yerba
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Description = model.Description,
                        Jar = model.Jar,
                        Score = model.Score,
                        Producer = await _context.Producer.FirstAsync(p => p.Id == model.Producer)
                    };
                    _context.Update(yerba);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!YerbaExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Yerbas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var yerba = await _context.Yerbas
                .Include(y => y.Producer)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (yerba == null)
            {
                return NotFound();
            }

            return View(yerba);
        }

        // POST: Yerbas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var yerba = await _context.Yerbas.SingleOrDefaultAsync(m => m.Id == id);
            _context.Yerbas.Remove(yerba);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool YerbaExists(int id)
        {
            return _context.Yerbas.Any(e => e.Id == id);
        }
    }
}
