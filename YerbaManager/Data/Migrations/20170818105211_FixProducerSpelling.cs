﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace YerbaManager.Data.Migrations
{
    public partial class FixProducerSpelling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Yerbas_Prodecers_ProdecerId",
                table: "Yerbas");

            migrationBuilder.DropTable(
                name: "Prodecers");

            migrationBuilder.RenameColumn(
                name: "ProdecerId",
                table: "Yerbas",
                newName: "ProducerId");

            migrationBuilder.RenameIndex(
                name: "IX_Yerbas_ProdecerId",
                table: "Yerbas",
                newName: "IX_Yerbas_ProducerId");

            migrationBuilder.CreateTable(
                name: "Producer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producer", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Yerbas_Producer_ProducerId",
                table: "Yerbas",
                column: "ProducerId",
                principalTable: "Producer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Yerbas_Producer_ProducerId",
                table: "Yerbas");

            migrationBuilder.DropTable(
                name: "Producer");

            migrationBuilder.RenameColumn(
                name: "ProducerId",
                table: "Yerbas",
                newName: "ProdecerId");

            migrationBuilder.RenameIndex(
                name: "IX_Yerbas_ProducerId",
                table: "Yerbas",
                newName: "IX_Yerbas_ProdecerId");

            migrationBuilder.CreateTable(
                name: "Prodecers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prodecers", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Yerbas_Prodecers_ProdecerId",
                table: "Yerbas",
                column: "ProdecerId",
                principalTable: "Prodecers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
